FROM amorid/workspace:8.3-bullseye

COPY workspace.bash /tmp/wks.bash

RUN bash /tmp/wks.bash

WORKDIR /var/www
