#!/usr/bin/env bash

SYMFONY_CLI_VERSION=5.10.5
COMPOSER_VER=latest-stable
PHP_CS_FIXER_VER=3.65.0
NODE_VERSION=18
SHELL_USERNAME=amorid
SHELL_UID=1000
SHELL_GID=1000
SHELL_HOME=/home/$SHELL_USERNAME

apt update
apt install unzip -y

wget https://github.com/symfony-cli/symfony-cli/releases/download/v${SYMFONY_CLI_VERSION}/symfony-cli_${SYMFONY_CLI_VERSION}_amd64.deb && \
    dpkg -i symfony-cli_${SYMFONY_CLI_VERSION}_amd64.deb && \
    rm symfony-cli_${SYMFONY_CLI_VERSION}_amd64.deb

curl -o /usr/local/bin/composer https://getcomposer.org/download/${COMPOSER_VER}/composer.phar && \
    chmod +x /usr/local/bin/composer

wget -O /usr/local/bin/php-cs-fixer https://github.com/PHP-CS-Fixer/PHP-CS-Fixer/releases/download/v${PHP_CS_FIXER_VER}/php-cs-fixer.phar && \
    chmod +x /usr/local/bin/php-cs-fixer

addgroup $SHELL_USERNAME --gid $SHELL_GID && \
    useradd -m --uid $SHELL_UID --gid $SHELL_GID --shell /bin/bash $SHELL_USERNAME && \
    adduser $SHELL_USERNAME www-data

export NVM_DIR=$SHELL_HOME/.nvm
mkdir -p $NVM_DIR
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash && \
    . $NVM_DIR/nvm.sh && \
    nvm install $NODE_VERSION && \
    npm install -g yarn

chown -R $SHELL_USERNAME: $NVM_DIR

echo "
export NVM_DIR=\"/home/$SHELL_USERNAME/.nvm\"
[ -s \"\$NVM_DIR/nvm.sh\" ] && \. \"\$NVM_DIR/nvm.sh\"
[ -s \"\$NVM_DIR/bash_completion\" ] && \. \"\$NVM_DIR/bash_completion\"
" >> $SHELL_HOME/.bashrc
