<?php

declare(strict_types=1);

/*
 * This file is part of AMORID Project
 *
 * (c) AMORID <https://malam.or.id>
 */

namespace App\Trait;

use Symfony\Component\HttpFoundation\Response;

trait SendTextResponseTrait
{
    public function responseAsText(?string $content = '', int $status = Response::HTTP_OK, array $headers = []): Response
    {
        $response = new Response($content, $status, $headers);
        $response->headers->set('Content-Type', 'text/plain');

        return $response;
    }
}
