<?php

declare(strict_types=1);

/*
 * This file is part of AMORID Project
 *
 * (c) AMORID <https://malam.or.id>
 */

namespace App\Controller;

use App\Trait\SendTextResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\WebLink\Link;
use Symfony\Contracts\Translation\TranslatorInterface;

class HomeController extends AbstractController
{
    use SendTextResponseTrait;

    #[Route('/', name: 'app_home', methods: ['GET'])]
    public function index(Request $request, TranslatorInterface $translator): Response
    {
        $nextLocale = match ($request->getLocale()) {
            'en' => 'id',
            'id' => 'en',
        };

        $thankyou = $translator->trans('Thank you for taking the time to visit malam.or.id.');

        $this->addLink($request, new Link(
            'alternate',
            $this->generateUrl('app_home', ['_locale' => $nextLocale], UrlGeneratorInterface::ABSOLUTE_URL),
        ));

        return $this->responseAsText($thankyou);
    }
}
