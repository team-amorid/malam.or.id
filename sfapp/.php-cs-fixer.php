<?php

declare(strict_types=1);

/*
 * This file is part of AMORID Project
 *
 * (c) AMORID <https://malam.or.id>
 */

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$header = <<<HEADER
This file is part of AMORID Project

(c) AMORID <https://malam.or.id>
HEADER;

$rules = [
    '@Symfony' => true,
    'mb_str_functions' => true,
    'no_useless_else' => true,
    'no_useless_return' => true,
    'phpdoc_order' => true,
    'strict_comparison' => true,
    'strict_param' => true,
    'ordered_class_elements' => true,
    'declare_strict_types' => true,
    'linebreak_after_opening_tag' => false,
    'header_comment' => ['header' => $header, 'separate' => 'both'],
    'global_namespace_import' => [
        'import_classes' => true,
        'import_constants' => true,
        'import_functions' => true,
    ],
];

$finder = Finder::create()
    ->in(__DIR__)
    ->exclude('config')
    ->exclude('var')
    ->exclude('public/bundles')
    ->exclude('public/build')
    ->notPath('bin/console')
    ->notPath('public/index.php')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true);

return (new Config())
    ->setFinder($finder)
    ->setRules($rules)
    ->setLineEnding("\n")
    ->setRiskyAllowed(true)
    ->setUsingCache(true)
    ->setCacheFile(__DIR__.'/var/.php-cs-fixer.cache')
;
